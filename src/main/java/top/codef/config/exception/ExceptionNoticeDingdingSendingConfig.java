package top.codef.config.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import top.codef.config.annos.ConditionalOnExceptionNotice;
import top.codef.config.notice.NoticeTextResolverCustomer;
import top.codef.properties.notice.PrometheusNoticeProperties;
import top.codef.text.NoticeTextResolverProvider;
import top.codef.text.markdown.ExceptionNoticeMarkdownMessageResolver;

@Configuration
@ConditionalOnExceptionNotice
public class ExceptionNoticeDingdingSendingConfig implements NoticeTextResolverCustomer {

	@Autowired
	private PrometheusNoticeProperties prometheusNoticeProperties;

//	private final Log logger = LogFactory.getLog(ExceptionNoticeDingdingSendingConfig.class);

	@Override
	public void custom(NoticeTextResolverProvider resolverProvider) {
		switch (prometheusNoticeProperties.getDingdingTextType()) {
		case MARKDOWN:
			resolverProvider.add(new ExceptionNoticeMarkdownMessageResolver());
			break;
		default:
			break;
		}
	}

	@Override
	public int getOrder() {
		return Ordered.LOWEST_PRECEDENCE;
	}

}
