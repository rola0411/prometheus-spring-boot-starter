package top.codef.pojos.servicemonitor;

public enum ServiceStatus {

	UP, DOWN, RECOVERED;
}
