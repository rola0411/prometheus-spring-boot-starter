package top.codef.text;

import top.codef.notice.INoticeSendComponent;
import top.codef.pojos.PromethuesNotice;

public class DefaultResolver implements NoticeTextResolver {

	@Override
	public String resolve(PromethuesNotice notice) {
		return notice.toString();
	}

	@Override
	public boolean support(Class<? extends PromethuesNotice> clazz, INoticeSendComponent noticeSendComponent) {
		return true;
	}

}
